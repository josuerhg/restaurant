package com.jrhg.restaurant;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SubproductAdapter extends RecyclerView.Adapter<SubproductAdapter.ViewHolder> {
    private ArrayList<Subproduct> mValues;
    private SubproductListFragment mContext;

    //Functions to detect whenever an item is checked or unchecked
    interface OnCheckListener {
        void onCheck(Subproduct subproduct);
        void onUncheck(Subproduct subproduct);
    }

    private OnCheckListener onItemClick;

    SubproductAdapter(ArrayList<Subproduct> subproductsList, SubproductListFragment context, @NonNull OnCheckListener onCheckListener) {
        this.mValues = subproductsList;
        this.mContext = context;
        this.onItemClick = onCheckListener;
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.subproduct_item, viewGroup, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        viewHolder.subproductName.setText(mValues.get(i).getName());
        viewHolder.subproductExtra.setText( "+ $" + String.valueOf(mValues.get(i).getExtra()) );

        final Subproduct currentSubproduct = mValues.get(i);

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (viewHolder).checkbox.setChecked(
                        !(viewHolder).checkbox.isChecked());
                if ((viewHolder).checkbox.isChecked()) {
                    onItemClick.onCheck(currentSubproduct);
                } else {
                    onItemClick.onUncheck(currentSubproduct);
                }
            }
        });
    }

    public int getItemCount() {
        return mValues.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView subproductName;
        TextView subproductExtra;
        RelativeLayout parentLayout;
        CheckBox checkbox;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            subproductName = itemView.findViewById(R.id.subproduct_name);
            subproductExtra = itemView.findViewById(R.id.subproduct_extra);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            checkbox = itemView.findViewById(R.id.subproduct_checkbox);
            checkbox.setClickable(false);
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }

}
