package com.jrhg.restaurant;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ProductAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ProductListActivity mParentActivity;
    private final ArrayList<Product> mValues;
    private final boolean mTwoPane;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Product item = (Product) view.getTag();
            //For tablets
            if (mTwoPane) {
                Bundle arguments = new Bundle();
                arguments.putString(SubproductListFragment.ARG_PRODUCT_NAME, item.getName());
                SubproductListFragment fragment = new SubproductListFragment();
                fragment.setArguments(arguments);
                mParentActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.product_detail_container, fragment)
                        .commit();
            } else {
                if((item.getSubproducts()).isEmpty()) {
                    Toast.makeText(view.getContext(), "You've added " + item.getName() + " to your cart!", Toast.LENGTH_SHORT).show();
                    ProductListActivity.updateShoppingCart("PAY: ", item.getPrice(), true);
                } else {
                    //For handhelds
                    Intent intent = new Intent(view.getContext(), SubproductListActivity.class);
                    intent.putExtra(SubproductListFragment.ARG_PRODUCT_NAME, item.getName());

                    view.getContext().startActivity(intent);
                }
            }
        }
    };

    ProductAdapter(ProductListActivity parent,
                   ArrayList<Product> products,
                   boolean twoPane) {
        mValues = products;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch(viewType) {
            case 1:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_item, parent, false);
                return new productWithSubproducts(view);

            case 2:
                View view2 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_item_without, parent, false);
                return new productWithoutSubproducts(view2);
        }
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        if (getItemViewType(i) == 1) {
            productWithSubproducts holder1 = (productWithSubproducts) viewHolder;
            holder1.nameView.setText(mValues.get(i).getName());
            holder1.priceView.setText("$" + String.valueOf(mValues.get(i).getPrice()));

            holder1.itemView.setTag(mValues.get(i));
            holder1.itemView.setOnClickListener(mOnClickListener);
        } else {
            productWithoutSubproducts holder2 = (productWithoutSubproducts) viewHolder;
            holder2.nameView.setText(mValues.get(i).getName());
            holder2.priceView.setText("$" + String.valueOf(mValues.get(i).getPrice()));

            holder2.itemView.setTag(mValues.get(i));
            holder2.itemView.setOnClickListener(mOnClickListener);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (!mValues.get(position).getSubproducts().isEmpty()) {
            return 1;
        } else {
            return 2;
        }
    }

    /*public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mIdView.setText(mValues.get(position).getName());
        holder.mContentView.setText("$" + String.valueOf(mValues.get(position).getPrice()));

        holder.itemView.setTag(mValues.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);
    }*/

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;
        final TextView mContentView;

        ViewHolder(View view) {
            super(view);
            mIdView = view.findViewById(R.id.product_name);
            mContentView = view.findViewById(R.id.product_price);
        }
    }

    class productWithSubproducts extends RecyclerView.ViewHolder {
        final TextView nameView;
        final TextView priceView;

        productWithSubproducts(View view) {
            super(view);
            nameView = view.findViewById(R.id.product_name);
            priceView = view.findViewById(R.id.product_price);
        }
    }

    class productWithoutSubproducts extends RecyclerView.ViewHolder {
        final TextView nameView;
        final TextView priceView;

        productWithoutSubproducts(View view) {
            super(view);
            nameView = view.findViewById(R.id.product_name);
            priceView = view.findViewById(R.id.product_price);
        }
    }
}