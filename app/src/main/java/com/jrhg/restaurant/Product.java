package com.jrhg.restaurant;

import java.util.ArrayList;

public class Product {
    private String name;
    private float price;
    private ArrayList<Subproduct> subproducts;

    //Default constructor
    Product() {
        this.name = "";
        this.price = (float) 0.0;
    }

    public String getName()
    {
        return name;
    }
    float getPrice() { return price; }
    ArrayList<Subproduct> getSubproducts() { return subproducts; }
}
