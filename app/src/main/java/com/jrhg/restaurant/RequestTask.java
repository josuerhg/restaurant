package com.jrhg.restaurant;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

//For the HTTP request. Much better as an AsyncTask
public class RequestTask extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... strings) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://altomobile.blob.core.windows.net/training/menu.json")
                .build();

        String json;

        Response response;
        try {
            response = client.newCall(request).execute();
            assert response.body() != null;
            json = response.body().string();

            return json;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
