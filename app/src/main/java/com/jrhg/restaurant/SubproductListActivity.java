package com.jrhg.restaurant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import static com.jrhg.restaurant.SubproductListFragment.ARG_PRODUCT_NAME;
import static com.jrhg.restaurant.SubproductListFragment.selectedSubproducts;


public class SubproductListActivity extends AppCompatActivity {

    float subproductTotal = (float) 0.0;
    float parentProductPrice = (float) 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subproduct_list);
        Toolbar toolbar = findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        //Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //savedInstanceState is non-null when there is fragment state
        if (savedInstanceState == null) {
            //Create the detail fragment and add it to the activity
            Bundle arguments = new Bundle();
            arguments.putString(ARG_PRODUCT_NAME,
                    getIntent().getStringExtra(ARG_PRODUCT_NAME));
            SubproductListFragment fragment = new SubproductListFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.product_detail_container, fragment)
                    .commit();
        }

        if(getIntent().hasExtra(ARG_PRODUCT_NAME))
            //Gets the parent product's price
            parentProductPrice = ProductListActivity
                                    .PRODUCT_MAP.get(getIntent().getStringExtra(ARG_PRODUCT_NAME))
                                    .getPrice();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //Go to the products activity
            navigateUpTo(new Intent(this, ProductListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //onClick method
    public void select(View view) {
        if(selectedSubproducts.isEmpty())
            Toast.makeText(this, "You haven't selected any subproducts.", Toast.LENGTH_SHORT).show();
        else {
            subproductTotal = computeSubproductTotal(selectedSubproducts);

            //TODO: show a list of all subproducts bought
            Toast.makeText(this, "Nice choice!", Toast.LENGTH_SHORT).show();

            selectedSubproducts.clear();

            //Send the subproducts total to the main activity (product list)
            Intent intent = new Intent(getBaseContext(), ProductListActivity.class);
            intent.putExtra("SUBPRODUCT_TOTAL", subproductTotal);
            navigateUpTo(intent);
        }
    }

    //Compute the total price of all selected subproducts
    private float computeSubproductTotal(ArrayList<Subproduct> subproducts) {

        float total = (float) 0.0;

        for(int i = 0; i < subproducts.size(); i++) {
            total += subproducts.get(i).getExtra() + parentProductPrice;
        }

        return total;
    }
}
