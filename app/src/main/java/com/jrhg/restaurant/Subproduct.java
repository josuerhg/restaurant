package com.jrhg.restaurant;

class Subproduct {
    private String name;
    private float extra;

    //Default constructor
    Subproduct() {
        this.name = "";
        this.extra = (float) 0.0;
    }

    public String getName() { return this.name; }
    public float getExtra() { return this.extra; }
}
