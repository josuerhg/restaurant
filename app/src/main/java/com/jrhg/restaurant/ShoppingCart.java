package com.jrhg.restaurant;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

//Alright, this is pretty important for the UI
//This Observable class allows its attributes to be "observed"
//and thus, to notify whenever they change
public class ShoppingCart extends BaseObservable {
    private String state;
    private Float totalPrice;
    private Boolean isActive;

    ShoppingCart() {
        this.state = "EMPTY CART";
        this.totalPrice = (float) 0.0;
        this.isActive = false;
    }

    //We use 'bindable' for automatic DataBinding
    @Bindable
    public String getState() { return this.state; }

    @Bindable
    public Float getTotalPrice() { return this.totalPrice; }

    @Bindable
    public Boolean getIsActive() { return this.isActive; }

    //BR helps us link the class and the layout, so that the changes on the UI are reflected automatically
    void setState(String state) { this.state = state; notifyPropertyChanged(com.jrhg.restaurant.BR.state); }
    void setTotalPrice(Float t) { this.totalPrice = t; notifyPropertyChanged(com.jrhg.restaurant.BR.totalPrice); }
    void setActive(Boolean i) { this.isActive = i; notifyPropertyChanged(com.jrhg.restaurant.BR.isActive); }

}
