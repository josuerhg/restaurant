package com.jrhg.restaurant;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class SubproductListFragment extends Fragment {

    public static final String ARG_PRODUCT_NAME = "product_name";
    private Product mItem;
    public static ArrayList<Subproduct> selectedSubproducts = new ArrayList<>();

    //Mandatory empty constructor
    public SubproductListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assert getArguments() != null;
        if (getArguments().containsKey(ARG_PRODUCT_NAME)) {
            //Using our HashMap (and the key we obtained from the main activity, we can find
            //the product we select in the product list
            mItem = ProductListActivity.PRODUCT_MAP.get(getArguments().getString(ARG_PRODUCT_NAME));

            Activity activity = this.getActivity();
            assert activity != null;
            CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getName() + " subproducts");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.subproduct_list, container, false);

        RecyclerView subproductsView = rootView.findViewById(R.id.subproduct_view);

        SubproductAdapter subproductAdapter = new SubproductAdapter(mItem.getSubproducts(), this, new SubproductAdapter.OnCheckListener() {
            @Override
            public void onCheck(Subproduct subproduct) {
                //whenever an item's checkbox is checked, it will be added to an ArrayList
                selectedSubproducts.add(subproduct);
            }

            @Override
            public void onUncheck(Subproduct subproduct) {
                //vice-versa
                selectedSubproducts.remove(subproduct);
            }
        });

        subproductsView.setAdapter(subproductAdapter);

        subproductsView.setLayoutManager(new LinearLayoutManager(getContext()));

        return rootView;
    }
}
