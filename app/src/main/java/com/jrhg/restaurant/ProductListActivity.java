package com.jrhg.restaurant;

import com.jrhg.restaurant.databinding.ActivityProductListBinding;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class ProductListActivity extends AppCompatActivity {

    //====================
    // Activity variables
    //====================
    private boolean mTwoPane;
    public ArrayList<Product> productsAL;
    public static Map<String, Product> PRODUCT_MAP = new HashMap<>();
    Bundle bundle;
    public static ShoppingCart shoppingCart = new ShoppingCart();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        //For tablets, this is included by default by the IDE
        if (findViewById(R.id.product_detail_container) != null) {
            mTwoPane = true;
        }

        //Setup databinding, to link the Shopping Cart's values and its XML styling
        ActivityProductListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_product_list);
        binding.setShoppingCart(shoppingCart);

        //Download JSON and parse it
        try {
            String json = new RequestTask().execute().get();
            productsAL = parseJson(json);
            PRODUCT_MAP = setupMap(productsAL);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        //Setup recycler view
        View recyclerView = findViewById(R.id.product_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        //Dividing lines for the recycler view
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        ((RecyclerView)recyclerView).addItemDecoration(itemDecor);

        //To obtain the 'subproduct total' value from SubproductListActivity
        if(checkForExtra()) {
            bundle = getIntent().getExtras();
            assert bundle != null;
            float subproductTotal = bundle.getFloat("SUBPRODUCT_TOTAL");
            updateShoppingCart("PAY: ", subproductTotal, true);
        }

    }


    //===============================================
    //           ADDITIONAL FUNCTIONS
    //===============================================

    //------------------JSON parser------------------------
    private ArrayList<Product> parseJson(String json) {
        Gson gson = new Gson();
        Type rootType = new TypeToken<Map<String, ArrayList<Product>>>(){}.getType();
        Map<String, ArrayList<Product>> root = gson.fromJson(json, rootType);
        return(root.get("products"));
    }

    //-------Create a HashMap for the Subproducts Activity------
    private HashMap<String, Product> setupMap (ArrayList<Product> arrayList) {
        HashMap<String,Product> map = new HashMap<>();

        for(int i = 0; i < arrayList.size(); i++) {
            map.put(arrayList.get(i).getName(), arrayList.get(i));
        }

        return map;
    }

    //--------------------Take a guess :^)------------------------------
    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new ProductAdapter(this, productsAL, mTwoPane));
    }

    //------------------Change the shopping cart's attributes------------
    public static void updateShoppingCart(String s, float t, boolean i) {
        shoppingCart.setTotalPrice(shoppingCart.getTotalPrice() + t);
        shoppingCart.setState(s);
        shoppingCart.setActive(i);
    }

    public static void resetCart() {
        shoppingCart.setTotalPrice((float) 0.0);
        shoppingCart.setState("EMPTY CART");
        shoppingCart.setActive(false);
    }


    //--------------------Confirm purchase-------------------------------
    public void buy(View view) {
        //if(!shoppingCart.getIsActive())
        if(!shoppingCart.getIsActive())
            Toast.makeText(this, "Hey, your cart is empty!", Toast.LENGTH_SHORT).show();
        else {
            //Show a confirmation dialog
            AlertDialog.Builder purchaseDialog = new AlertDialog.Builder(this);
            purchaseDialog.setMessage("This is actually a real app and you just gave me: $" + shoppingCart.getTotalPrice())
                    .setTitle(R.string.purchase_title)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //dismiss
                        }
                    });
            purchaseDialog.show();

            resetCart();
        }
    }

    //--------------------Reset button-------------------------
    public void resetButton(MenuItem item) {
        AlertDialog.Builder purchaseDialog = new AlertDialog.Builder(this);
        purchaseDialog.setMessage("Your cart has been reset successfully!")
                .setTitle("Cart reset!")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //dismiss
                    }
                });
        purchaseDialog.show();
        resetCart();
    }

    //----Check if an extra by passed by the SubproductList activity-------
    boolean checkForExtra() {
        return getIntent().hasExtra("SUBPRODUCT_TOTAL");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.app_menu, menu);
        return true;
    }

}
